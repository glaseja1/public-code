class FibbonaciHeapNode {
    constructor(v) {
        this.child = null;
        this.degree = 0;
        this.marked = false;
        this.next = null;
        this.prev = null;
        this.parent = null;
        this.value = v;
        this.next = this;
        this.prev = this;
    }
    addChild(n) {
        n.parent = this;
        this.degree++;
        if (this.child !== null)
            this.child.insertToCircular(n);
        else
            this.child = n;
    }
    hasChildren() { return this.child !== null; }
    hasParent() { return this.parent !== null; }
    insertToCircular(t) {
        this.prev.next = t;
        t.next = this;
        t.prev = this.prev;
        this.prev = t;
    }
    insertListToCircular(t) {
        this.prev.next = t;
        t.prev.next = this;
        var last = t.prev;
        t.prev = this.prev;
        this.prev = last;
    }
    removeFromCircularList() {
        if (this.next == this)
            return;
        this.prev.next = this.next;
        this.next.prev = this.prev;
        this.next = this;
        this.prev = this;
    }
}
class DegreeArr {
    constructor() {
        this.arr = new Array();
    }
    contains(key) { return this.arr[`${key}`] !== undefined; }
    delete(key) {
        if (!this.contains(key))
            return false;
        delete this.arr[key];
        return true;
    }
    get(key) {
        if (!this.contains(key))
            throw new Error(`get(): Non existent key '${key}' in FibbonaciHeapNode`);
        return this.arr[key];
    }
    insert(key, value) {
        if (this.contains(key))
            return false;
        this.arr[key] = value;
        return true;
    }
}
class FibbonaciHeapBase {
    constructor(comparator, compareResultForMinMax) {
        this._length = 0;
        this.minMaxPtr = null;
        if (compareResultForMinMax !== -1 && compareResultForMinMax !== 1)
            throw new Error(`compareResultForMinMax: must be -1 or 1`);
        this.comparator = comparator;
        this.compareResultForMinMax = compareResultForMinMax;
    }
    clear() {
        this._length = 0;
        this.minMaxPtr = null;
    }
    cascadeCut(node) {
        if (!node.hasParent())
            return;
        if (!node.marked) {
            if (node.hasParent()) {
                node.marked = true;
            }
        }
        else {
            var parentBackup = node.parent;
            this.cut(node);
            this.cascadeCut(parentBackup);
        }
    }
    cut(node) {
        if (node.parent.degree == 1) {
            node.parent.degree--;
            node.parent.child = null;
            node.parent = null;
            node.marked = false;
            this.minMaxPtr.insertToCircular(node);
            return;
        }
        node.parent.degree--;
        node.parent.child = node.next;
        node.removeFromCircularList();
        node.marked = false;
        this.minMaxPtr.insertToCircular(node);
    }
    decreaseIncreaseKey_inner(node, newVal) {
        if (this.comparator(node.value, newVal) === this.compareResultForMinMax)
            throw new Error("Undefined behavior");
        if (this._length == 0)
            throw new Error("The heap is empty");
        if (this.comparator(node.value, newVal) === 0)
            return;
        node.value = newVal;
        if (node.parent === null ||
            (node.parent !== null && this.comparator(node.parent.value, newVal) === this.compareResultForMinMax)) {
            if (node.parent === null)
                this.updateMinMaxPtrIfNeeded(node);
            return;
        }
        var cutNodeParent = node.parent;
        this.cut(node);
        this.cascadeCut(cutNodeParent);
        this.updateMinMax();
    }
    extractMinMax() {
        if (this._length == 0)
            throw new Error("The heap is empty");
        var val = this.minMaxPtr.value;
        var removedDegree = this.minMaxPtr.degree;
        if (this.minMaxPtr.hasChildren())
            this.removeMinMaxAndAddChildrenToRoot();
        else {
            this.minMaxPtr = this.minMaxPtr.next;
            this.minMaxPtr.prev.removeFromCircularList();
        }
        this.updateMinMax();
        this.mergeTrees();
        this._length--;
        return val;
    }
    find(v) {
        return this.findRec(v, this.minMaxPtr);
    }
    findRec(v, root) {
        var tmp = root;
        do {
            if (this.comparator(tmp.value, v) === 0)
                return tmp;
            if (tmp.child !== null) {
                var resXX = this.findRec(v, tmp.child);
                if (resXX !== null)
                    return resXX;
            }
            tmp = tmp.next;
        } while (tmp !== root);
        return null;
    }
    insert(v) {
        let t = new FibbonaciHeapNode(v);
        t.prev = t;
        t.next = t;
        if (this.minMaxPtr !== null) {
            this.minMaxPtr.insertToCircular(t);
            if (this.comparator(v, this.minMaxPtr.value) === this.compareResultForMinMax)
                this.minMaxPtr = t;
        }
        else
            this.minMaxPtr = t;
        this._length++;
        return t;
    }
    isEmpty() { return this._length == 0; }
    length() { return this._length; }
    mergeTrees() {
        var arr = new DegreeArr();
        var curr = this.minMaxPtr;
        for (let i = 0; i < this._length - 1; i++) {
            if (arr.contains(curr.degree) && curr === arr.get(curr.degree))
                return;
            curr = this.mergeTreesProcessNodeRecursive(curr, arr);
        }
    }
    mergeTreesProcessNodeRecursive(curr, hs) {
        if (hs.insert(curr.degree, curr))
            return curr.next;
        var a = curr;
        var b = hs.get(curr.degree);
        hs.delete(curr.degree);
        var parent = a;
        var child = b;
        if (this.comparator(a.value, b.value) === 0) {
            if (this.minMaxPtr === a) {
                parent = a;
                child = b;
            }
            else {
                parent = b;
                child = a;
            }
        }
        else if (this.comparator(a.value, b.value) !== this.compareResultForMinMax) {
            parent = b;
            child = a;
        }
        child.removeFromCircularList();
        var future = parent.next;
        parent.addChild(child);
        if (!hs.insert(parent.degree, parent)) {
            return this.mergeTreesProcessNodeRecursive(parent, hs);
        }
        return future;
    }
    removeMinMaxAndAddChildrenToRoot() {
        if (this.minMaxPtr == this.minMaxPtr.next) {
            this.minMaxPtr.child.parent = null;
            this.minMaxPtr = this.minMaxPtr.child;
        }
        else {
            var chosenChild = this.minMaxPtr.child;
            this.minMaxPtr = this.minMaxPtr.next;
            this.minMaxPtr.prev.removeFromCircularList();
            this.minMaxPtr.insertListToCircular(chosenChild);
        }
    }
    toString() {
        if (this._length == 0)
            return "The heap is empty";
        var curr = this.minMaxPtr.next;
        var s = `value [${this.minMaxPtr.value}] (degree ${this.minMaxPtr.degree})`;
        while (curr !== this.minMaxPtr) {
            s += `, ${curr.value}`;
            curr = curr.next;
        }
        return s;
    }
    updateMinMax() {
        var tmp = this.minMaxPtr.next;
        while (tmp !== this.minMaxPtr) {
            tmp.parent = null;
            if (this.comparator(tmp.value, this.minMaxPtr.value) === this.compareResultForMinMax) {
                this.minMaxPtr = tmp;
            }
            tmp = tmp.next;
        }
    }
    updateMinMaxPtrIfNeeded(candidate) {
        if (this.comparator(candidate.value, this.minMaxPtr.value) === this.compareResultForMinMax) {
            this.minMaxPtr = candidate;
        }
    }
}
class FibbonaciMinHeap extends FibbonaciHeapBase {
    constructor(comparator) {
        super(comparator, -1);
    }
    decreaseKey(node, newVal) {
        this.decreaseIncreaseKey_inner(node, newVal);
    }
    increaseKey(node, newVal, MIN_VALUE) {
        if (this._length == 0)
            throw new Error("The heap is empty");
        if (node.value == newVal)
            return;
        if (node.value > newVal)
            throw new Error("Undefined behavior");
        this.decreaseKey(node, MIN_VALUE);
        this.extractMinMax();
        this.insert(newVal);
    }
    extractMin() { return this.extractMinMax(); }
}
//# sourceMappingURL=minHeapBundle.js.map