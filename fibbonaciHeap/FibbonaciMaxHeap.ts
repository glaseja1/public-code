/**
 * Jan Glaser
 * v. 1.0.0 - stable
 *
 * You are free to use this, guys :).
 * If you want to support me, here is a game I am developing
 * http://dol.glaser.cz
 * we are also on indieDB
 * https://www.indiedb.com/games/darkness-or-light
**/

///<reference path="./FibbonaciHeapBase.ts" />

/**
 * Represents a fibbonaci heap container,
 * which allows to always extract minimum.
 *
 * This is a MAX heap.
 * ALERT: if you use bad compare function, then behavior can be undefined
 *
 * Note: This is only user interface API, with nice function names,
 * relevant to the MIN / MAX heap.
 * The main functionality is not changed by this,
 * with exception of the comparism sign
 */
class FibbonaciMaxHeap<V> extends FibbonaciHeapBase<V>{
  /**
   * [constructor description]
   * @param comparator Compare function, which returns:
   * 0 - if elements A and B are considered equal
   * 1 - if element A is supposed to be after B
   * -1 - if element B is supposed to be after A
   *
   * AND NOTHING ELSE! If you return some other value, behavior is UNDEFINED
   */
  public constructor(comparator: (a: V, b: V) => number) {
    //-1 because we are min heap
    super(comparator, 1);
  }
  /**
   * Increases a key to a new value
   *
   * Note: this is ,,decreaseKey" functon by standards, but
   * with different comparism, and running on a max heap...
   *
   * @param node   [description]
   * @param newVal [description]
   */
  public increaseKey(node: FibbonaciHeapNode<V>, newVal: V): void {
    this.decreaseIncreaseKey_inner(node, newVal);
  }
  /**
   * Algorithmically more complex than increaseKey (since we are in MAX heap)
   * @param node      [description]
   * @param newVal    [description]
   * @param MIN_VALUE A value, which is considered as the maximal possible value of the type you are using...
   */
  public decreaseKey(node: FibbonaciHeapNode<V>, newVal: V, MAX_VALUE: V): void {
    if (this._length == 0)
      throw new Error("The heap is empty");
    if (node.value == newVal)
      return;
    if (node.value < newVal)
      throw new Error("Undefined behavior");

    //decrease key to -inf
    //delete min
    //insert
    this.increaseKey(node, MAX_VALUE);
    this.extractMinMax();
    this.insert(newVal);
  }
  /**
   * Removes the maximum from the heap, and returns it
   * @return [description]
   */
  public extractMax(): V { return this.extractMinMax(); }
}
