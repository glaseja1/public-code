/**
 * Jan Glaser
 * v. 1.0.0 - stable
 *
 * You are free to use this, guys :).
 * If you want to support me, here is a game I am developing
 * http://dol.glaser.cz
 * we are also on indieDB
 * https://www.indiedb.com/games/darkness-or-light
**/

/**
 * Represents a node for fibbonaci heap
 *
 * Definition:
 * Degree of a tree:
 * Let T be a tree. Then Degree of tree T is number of children of a root.
 *
 * exported for being able to test it
 *
 *
 * If you change any property of the node from outside, and you do not know what you are
 * doing, then the behavior is UNDEFINED
 * @return [description]
 */
class FibbonaciHeapNode<V>
{
  /**
   * Reference to any child of the childs of this node.
   * Note: Children are also circular linked list
   */
  public child: FibbonaciHeapNode<V> = null;
  /**
   * The degree property of the fibbonacci heap.
   * Degree is the number of children this node posseses
   */
  public degree: number = 0;
  /**
   * Whether the node is marked (property of a fibbonaci node)
   * Node is marked, if it has lost a child, and is not in a root
   */
  public marked: boolean = false;
  /**
   * The next node of this node, or reference to itself (the circular linked list)
   */
  public next: FibbonaciHeapNode<V> = null;
  /**
   * The previous node of this node, or reference to itself (the circular linked list)
   */
  public prev: FibbonaciHeapNode<V> = null;
  /**
   * The parent of this node, if any
   */
  public parent: FibbonaciHeapNode<V> = null;
  /**
   * The value held in the node
   */
  public value: V;
  public constructor(v: V) {
    this.value = v;
    this.next = this;
    this.prev = this;
  }
  /**
   * Adds a child to this node.
   * Also, the degree of this node is increased
   * @param n [description]
   */
  public addChild(n: FibbonaciHeapNode<V>): void {
    n.parent = this;
    this.degree++;

    if (this.child !== null)
      this.child.insertToCircular(n);
    else
      this.child = n;
  }
  public hasChildren(): boolean { return this.child !== null; }
  public hasParent(): boolean { return this.parent !== null; }
  /**
   * Considering, that this node is ALWAYS part of circular double way linked list,
   * adds node N to this linked list, in a way that N is before this node
   * @param  t Node to add
   * @return   [description]
   */
  public insertToCircular(t: FibbonaciHeapNode<V>): void {
    this.prev.next = t;
    t.next = this;
    t.prev = this.prev;
    this.prev = t;
  }
  /**
   * Considering, that this node is ALWAYS part of circular double way linked list,
   * adds an entire linked list to this linked list, in a way that the inserted list is before this node
   * @param  t Node to add
   * @return   [description]
   */
  public insertListToCircular(t: FibbonaciHeapNode<V>): void {
    //  console.log(t.value)
    this.prev.next = t;
    t.prev.next = this;
    var last: FibbonaciHeapNode<V> = t.prev;
    t.prev = this.prev;
    this.prev = last;
    /* console.log('prev of t: ' + t.prev.value)
      console.log('next of t: ' + t.next.value)
      console.log('prev of this: ' + this.prev.value)
      console.log('next of this: ' + this.next.value)*/
  }
  /**
   * Unlinks this node from the left-right circular linked list
   */
  public removeFromCircularList(): void {
    if (this.next == this)
      return;
    this.prev.next = this.next;
    this.next.prev = this.prev;
    //create cycle
    this.next = this;
    this.prev = this;
  }
}

/**
 * Represents simplistic container for maintaining trees by degree.
 * This is esentially ripped off hashmap
 */
class DegreeArr<T> {
  private arr = new Array<T>();
  public contains(key: number): boolean { return this.arr[`${key}`] !== undefined; }
  /**
   * [delete description]
   * @param  key [description]
   * @return     Whetehr the element was found and deleted
   */
  public delete(key: number): boolean {
    if (!this.contains(key))
      return false;
    delete this.arr[key];
    return true;
  }
  public get(key: number): T {
    if (!this.contains(key))
      throw new Error(`get(): Non existent key '${key}' in FibbonaciHeapNode`);
    return this.arr[key];
  }
  /**
   * Inserts new element. If the key already exists,
   * element is not inserted and false is returned
   * @param  key   [description]
   * @param  value [description]
   * @return       [description]
   */
  public insert(key: number, value: T): boolean {
    if (this.contains(key))
      return false;
    this.arr[key] = value;
    return true;
  }
}
abstract class FibbonaciHeapBase<V> {
  /**
   * function for comparing the elements
   */
  protected comparator: (a: V, b: V) => number;
  protected compareResultForMinMax: number;
  protected _length: number = 0;
  /**
   * protected for being able to mock it and test the internal representation
   */
  protected minMaxPtr: FibbonaciHeapNode<V> = null;

  public constructor(comparator: (a: V, b: V) => number, compareResultForMinMax: number) {
    if (compareResultForMinMax !== -1 && compareResultForMinMax !== 1)
      throw new Error(`compareResultForMinMax: must be -1 or 1`);
    this.comparator = comparator;
    this.compareResultForMinMax = compareResultForMinMax;
  }
  public clear(): void {
    this._length = 0;
    this.minMaxPtr = null;
  }
  /**
   * Recursive cutting function
   * @param node [description]
   */
  protected cascadeCut(node: FibbonaciHeapNode<V>): void {
    //console.log("cascade cut on " + node.value);
    if (!node.hasParent())
      return;
    if (!node.marked) {
      //do not mark node if it is a root
      if (node.hasParent()) {
        //console.log("marking" + node.value);
        node.marked = true;
      }
    }
    else {
      var parentBackup = node.parent;
      this.cut(node);
      this.cascadeCut(parentBackup);
    }
  }
  /**
   * Let N be node in the argument.
   * Then, N is disconnected from his parent and placed into the root list
   * @param node [description]
   */
  protected cut(node: FibbonaciHeapNode<V>): void {
    //console.log("cutting" + node.value);
    if (node.parent.degree == 1) {
      node.parent.degree--;
      node.parent.child = null;
      node.parent = null;
      //clear mark
      node.marked = false;
      this.minMaxPtr.insertToCircular(node);
      return;
    }
    node.parent.degree--;
    //if parent has more children
    //avoid problem if parent would have exactly this node as a child
    node.parent.child = node.next;

    //disconnect the node from list
    node.removeFromCircularList();

    //clear mark
    node.marked = false;
    this.minMaxPtr.insertToCircular(node);
  }
  /**
   * Protected function, contains common part for all (min / max) heaps
   * Decreases / increases a key to a new value.
   * The key is decreased in case of MIN heap,
   * and increased in case of MAX heap.
   *
   * All in all, this is ,,decreaseKey" operation, which is the same, if you
   * transition to max heap....it just shall be called increase key...
   *
   * But the function naming is corrent in the specialized classes
   * @param node   [description]
   * @param newVal [description]
   */
  protected decreaseIncreaseKey_inner(node: FibbonaciHeapNode<V>, newVal: V): void {
    //the only thing to do here is check whether we are really decreasing...
    if (this.comparator(node.value, newVal) === this.compareResultForMinMax)
      throw new Error("Undefined behavior");

    if (this._length == 0)
      throw new Error("The heap is empty");
    //if they are equal
    if (this.comparator(node.value, newVal) === 0)
      return;

    node.value = newVal;

    //case1: The heap property will not be violated
    if (node.parent === null ||
      (node.parent !== null && this.comparator(node.parent.value, newVal) === this.compareResultForMinMax)) {
      /*if (node.parent !== null)
        console.log(`heap property will not be violated: (parent value: ${node.parent.value}, the changed child's value: ${newVal})`)
      else
        console.log(`heap property will not be violated: the changed child's value: ${newVal})`);
    */
      if (node.parent === null)
        this.updateMinMaxPtrIfNeeded(node);
      //done
      return;
    }
    //if we got here, the heap property is violated
    //cut the node, and add it to the rootlist

    //save parent of the cut node
    var cutNodeParent = node.parent;
    this.cut(node);
    this.cascadeCut(cutNodeParent);
    //updateMinPtrIfNeeded
    this.updateMinMax(); //TODO theoretically could be faster...
    //console.log('done')
  }
  public extractMinMax(): V {
    //console.log()
    //console.log(`extractMinMax: `)
    if (this._length == 0)
      throw new Error("The heap is empty");

    var val: V = this.minMaxPtr.value;
    var removedDegree: number = this.minMaxPtr.degree;

    if (this.minMaxPtr.hasChildren())
      this.removeMinMaxAndAddChildrenToRoot();
    else {
      //advance to next element
      this.minMaxPtr = this.minMaxPtr.next;
      this.minMaxPtr.prev.removeFromCircularList();
    }
    this.updateMinMax();
    this.mergeTrees();
    this._length--;
    return val;
  }
  /**
   * Returns any node N, which has the value requested.
   * If no such node is found, null is returned
   *
   * Used exclusivelly by testingsuites. DO NOT USE THIS IN REAL APP,
   * because it is so slow (because of the definition of the heap....)
   * @param  v [description]
   * @return   [description]
   */
  public find(v: V): FibbonaciHeapNode<V> {
    //console.log('find ' + v)
    //and here we fucking iterate...
    return this.findRec(v, this.minMaxPtr);
  }
  private findRec(v: V, root: FibbonaciHeapNode<V>): FibbonaciHeapNode<V> {
    //console.log('findRec')
    var tmp = root;
    do {
      if (this.comparator(tmp.value, v) === 0)
        return tmp;
      //console.log(tmp.value)
      if (tmp.child !== null) {
        var resXX = this.findRec(v, tmp.child);
        if (resXX !== null)
          return resXX;
      }
      tmp = tmp.next;
    } while (tmp !== root)
    return null;
  }
  /**
   * [insert description]
   * @param  v [description]
   * @return   Pointer to the node. Can be used for decreasing key.
   * Do not change any property of the node by yourself. If you do, then behavior
   * is undefined
   */
  public insert(v: V): FibbonaciHeapNode<V> {
    //console.log(`insert ${v}`);
    let t: FibbonaciHeapNode<V> = new FibbonaciHeapNode<V>(v);
    t.prev = t;
    t.next = t;
    if (this.minMaxPtr !== null) {
      this.minMaxPtr.insertToCircular(t);
      //if the element is smaller / greater than actual min / max
      if (this.comparator(v, this.minMaxPtr.value) === this.compareResultForMinMax)
        this.minMaxPtr = t;
    }
    else
      this.minMaxPtr = t;
    this._length++;
    return t;
  }
  public isEmpty(): boolean { return this._length == 0; }
  public length(): number { return this._length; }

  private mergeTrees(): void {
    //create degree array
    var arr = new DegreeArr<FibbonaciHeapNode<V>>();
    var curr: FibbonaciHeapNode<V> = this.minMaxPtr;
    for (let i: number = 0; i < this._length - 1; i++) {
      //if the node would be merged with itself...
      if (arr.contains(curr.degree) && curr === arr.get(curr.degree))
        return;
      curr = this.mergeTreesProcessNodeRecursive(curr, arr);
    }
  }
  /**
   * Given node, checks if there is a degree in the lookup array.
   * If there is, it merges such a trees.
   * If even afterwards, there exists a degree clas, calls itself recursivelly.
   * @param  curr [description]
   * @param  hs   [description]
   * @return      next node which shall be processed
   */
  private mergeTreesProcessNodeRecursive(curr: FibbonaciHeapNode<V>, hs: DegreeArr<FibbonaciHeapNode<V>>): FibbonaciHeapNode<V> {
    //console.log(`\tprocessing ${curr.value}`);
    if (hs.insert(curr.degree, curr))
      return curr.next;
    var a: FibbonaciHeapNode<V> = curr;
    var b: FibbonaciHeapNode<V> = hs.get(curr.degree);
    //console.log(`have to merge trees ${a.value} and ${b.value}`);
    hs.delete(curr.degree);
    var parent = a;
    var child = b;

    //special case for duplicities
    if (this.comparator(a.value, b.value) === 0) {
      //always make the parent the one node, which is actually the pointer to minmax
      if (this.minMaxPtr === a) {
        parent = a;
        child = b;
      }
      else {
        parent = b;
        child = a;
      }
    }
    else if (this.comparator(a.value, b.value) !== this.compareResultForMinMax) {
      parent = b;
      child = a;
    }
    //console.log(`merging trees: parent; ${parent.value}, child ${child.value}`);
    child.removeFromCircularList();
    var future: FibbonaciHeapNode<V> = parent.next;
    parent.addChild(child);
    if (!hs.insert(parent.degree, parent)) {
      //  console.log(`another degree clash with ${hs.get(parent.degree).value}`);
      return this.mergeTreesProcessNodeRecursive(parent, hs);
    }
    return future;
  }
  private removeMinMaxAndAddChildrenToRoot(): void {
    //console.log('removed node has children')
    if (this.minMaxPtr == this.minMaxPtr.next) {
      this.minMaxPtr.child.parent = null;
      this.minMaxPtr = this.minMaxPtr.child;
    }
    else {
      var chosenChild = this.minMaxPtr.child;
      this.minMaxPtr = this.minMaxPtr.next;
      this.minMaxPtr.prev.removeFromCircularList();
      this.minMaxPtr.insertListToCircular(chosenChild);
    }
  }
  public toString(): string {
    if (this._length == 0)
      return "The heap is empty";
    var curr: FibbonaciHeapNode<V> = this.minMaxPtr.next;
    var s: string = `value [${this.minMaxPtr.value}] (degree ${this.minMaxPtr.degree})`;
    while (curr !== this.minMaxPtr) {
      //  s += `, value [${curr.value}] (degree ${curr.degree})`;
      s += `, ${curr.value}`;
      curr = curr.next;
    }
    return s;
  }
  protected updateMinMax(): void {
    //console.log('updateMinMax()')
    //update min/max
    var tmp: FibbonaciHeapNode<V> = this.minMaxPtr.next;
    while (tmp !== this.minMaxPtr) {
      //clear parent
      tmp.parent = null;
      if (this.comparator(tmp.value, this.minMaxPtr.value) === this.compareResultForMinMax) {
        this.minMaxPtr = tmp;
        //console.log(`new minMaxPtr: ` + this.minMaxPtr.value)
      }
      tmp = tmp.next;
    }
  }
  /**
   * @param candidate The candidate for new min / max
   */
  protected updateMinMaxPtrIfNeeded(candidate: FibbonaciHeapNode<V>): void {
    if (this.comparator(candidate.value, this.minMaxPtr.value) === this.compareResultForMinMax) {
      this.minMaxPtr = candidate;
      //console.log(`new minMaxPtr: ` + this.minMaxPtr.value)
    }
  }
}
