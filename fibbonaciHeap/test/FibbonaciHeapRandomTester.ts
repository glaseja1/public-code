import { assert, expect } from 'chai';
import { FibbonaciHeapBase, FibbonaciHeapNode } from "@FibbonaciHeapBase";
import { FibbonaciMinHeap } from "@FibbonaciMinHeap";
import { Functions } from "@Functions";

export class FibbonaciHeapRandomTester {

  private comparator: (a: number, b: number) => number;
  private _reportInput: boolean = false;

  public constructor() {
    this.comparator = function(a: number, b: number): number {
      if (a == b) return 0;
      if (a > b)
        return 1;
      return -1;
    };
  }
  public testWithRandomData(numberOfDataToGenerate: number,
    randomizeNumberOfData: boolean = false): void {
    var A = new FibbonaciMinHeap<number>(this.comparator);
    var arr = []

    if (randomizeNumberOfData)
      numberOfDataToGenerate = Functions.generateRandomNumber(0, numberOfDataToGenerate);
    //An array of inputs, chronologically how they were inserted into the container.
    var inputString: string = "";
    //fill with data
    for (let i: number = 0; i < numberOfDataToGenerate; i++) {
      let num: number = Functions.generateRandomNumber(-1000, 1000);
      inputString += ` ${num}`;
      //console.log(`insert ${num}`);
      arr.push(num);
      A.insert(num);
    }/*
    if (this._reportInput) {
      console.log("input is:")
      console.log(inputString);
      console.log()
    }*/
    arr.sort(this.comparator);

    assert.equal(arr.length, A.length());
    //start remove / increase / decrease
    var randomOps = 50;
    for (let i: number = 0; i < randomOps; i++) {
      arr = this.performRandomOperation(arr, A);
      if (arr.length == 0)
        break;
    }
  }
  private performRandomOperation(arr: Array<number>, A: FibbonaciMinHeap<number>): Array<number> {
    var rand = Math.random();
    if (rand < 1 / 2) {
      var e: number = A.extractMinMax();
      //console.log(`extracting ${arr[0]}`);
      assert.equal(arr[0], e, `Container did not provide sorted element of value: ${arr[0]}`);
      arr.splice(0, 1);
    } else {
      //chose random number to change
      var index = Functions.generateRandomNumber(0, arr.length - 1);

      var from: number = arr[index];
      var to: number = arr[index] - Functions.generateRandomNumber(0, 50);
      //console.log(`changing ${from} to ${to}`);
      //find the node
      var nodeRef = A.find(from);
      if (nodeRef == null) {
        assert.fail(`Node with value ${from} was not found in the heap. What now...?`);
      }
      A.decreaseKey(nodeRef, to);
      arr[index] = to;
      //resort
      arr.sort(this.comparator);
    }
    return arr;
  }
}
import { assert, expect } from 'chai';
import { FibbonaciHeapBase, FibbonaciHeapNode } from "@FibbonaciHeapBase";
import { FibbonaciMinHeap } from "@FibbonaciMinHeap";
import { Functions } from "@Functions";

export class FibbonaciHeapRandomTester {

  private comparator: (a: number, b: number) => number;
  private _reportInput: boolean = false;

  public constructor() {
    this.comparator = function(a: number, b: number): number {
      if (a == b) return 0;
      if (a > b)
        return 1;
      return -1;
    };
  }
  public testWithRandomData(numberOfDataToGenerate: number,
    randomizeNumberOfData: boolean = false): void {
    var A = new FibbonaciMinHeap<number>(this.comparator);
    var arr = []

    if (randomizeNumberOfData)
      numberOfDataToGenerate = Functions.generateRandomNumber(0, numberOfDataToGenerate);
    //An array of inputs, chronologically how they were inserted into the container.
    var inputString: string = "";
    //fill with data
    for (let i: number = 0; i < numberOfDataToGenerate; i++) {
      let num: number = Functions.generateRandomNumber(-1000, 1000);
      inputString += ` ${num}`;
      //console.log(`insert ${num}`);
      arr.push(num);
      A.insert(num);
    }/*
    if (this._reportInput) {
      console.log("input is:")
      console.log(inputString);
      console.log()
    }*/
    arr.sort(this.comparator);

    assert.equal(arr.length, A.length());
    //start remove / increase / decrease
    var randomOps = 50;
    for (let i: number = 0; i < randomOps; i++) {
      arr = this.performRandomOperation(arr, A);
      if (arr.length == 0)
        break;
    }
  }
  private performRandomOperation(arr: Array<number>, A: FibbonaciMinHeap<number>): Array<number> {
    var rand = Math.random();
    if (rand < 1 / 2) {
      var e: number = A.extractMinMax();
      //console.log(`extracting ${arr[0]}`);
      assert.equal(arr[0], e, `Container did not provide sorted element of value: ${arr[0]}`);
      arr.splice(0, 1);
    } else {
      //chose random number to change
      var index = Functions.generateRandomNumber(0, arr.length - 1);

      var from: number = arr[index];
      var to: number = arr[index] - Functions.generateRandomNumber(0, 50);
      //console.log(`changing ${from} to ${to}`);
      //find the node
      var nodeRef = A.find(from);
      if (nodeRef == null) {
        assert.fail(`Node with value ${from} was not found in the heap. What now...?`);
      }
      A.decreaseKey(nodeRef, to);
      arr[index] = to;
      //resort
      arr.sort(this.comparator);
    }
    return arr;
  }
}

