import { assert, expect } from 'chai';
import { FibbonaciHeapBase, FibbonaciHeapNode } from "@FibbonaciHeapBase";
import { HeapTesterSuite } from "@HeapTesterSuite";
import { FibbonaciHeapRandomTester } from "@FibbonaciHeapRandomTester";
import { FibbonaciMinHeap } from "@FibbonaciMinHeap";

class FibbonaciHeapMock extends FibbonaciMinHeap<number>
{
  public constructor(comparator: (a: number, b: number) => number) { super(comparator); }
  public getMinPtr(): FibbonaciHeapNode<number> { return this.minMaxPtr }
  /**
   * Checks for correct order and values of roots of all trees currently in the heap
   * Do not call this if the heap is empty...
   * @param rootValues [description]
   */
  public requireRoots(rootValues: Array<number>): void {
    //scrape all roots into array
    var scraped = new Array<number>();
    var tmp = this.minMaxPtr;
    var i: number = 0;
    do {
      scraped.push(tmp.value);
      tmp = tmp.next;
    }
    while (tmp !== this.minMaxPtr)

    //sort both arrays
    scraped.sort();
    rootValues.sort();
    assert.equal(scraped.length, rootValues.length, `Not all of requested roots ${rootValues} were found`);

    for (let i = 0; i < rootValues.length; i++)
      assert.equal(rootValues[i], scraped[i], `Root at position ${i} should be ${rootValues[i]}, got ${tmp.value}`);
  }
}
//tests the MIN heap. But keep in mind, that if tests pass,
//then the core functionality is already tested for max heap aswell, since they share a base...
describe('FibbonaciHeap CORE test (MIN heap)', () => {
  var comparator: (a: number, b: number) => number = function(a: number, b: number): number {
    if (a == b) return 0;
    if (a > b)
      return 1;
    return -1;
  }
  //these tests were used while developing the code...
  describe('Low-level test', () => {
    describe('FibbonaciHeapNode test', () => {
      it('contructor()', () => {
        var N: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(1);
        assert.equal(N.parent, null);
        assert.equal(N.hasParent(), false);
        assert.equal(N.child, null);
        assert.equal(N.hasChildren(), false);
        assert.equal(N.next, N);
        assert.equal(N.prev, N);
        assert.equal(N.degree, 0);
      });
      it('addChild()', () => {
        var N: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(1);
        var N2: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(1);

        N.addChild(N2);
        assert.equal(N.parent, null);
        assert.equal(N.hasParent(), false);
        assert.equal(N2.parent, N);
        assert.equal(N2.hasParent(), true);

        assert.equal(N.child, N2);
        assert.equal(N.hasChildren(), true);
        assert.equal(N.degree, 1);

        //*********************
        var N3: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(1);
        N.addChild(N3);
        assert.equal(N.parent, null);
        assert.equal(N.hasParent(), false);
        assert.equal(N3.parent, N);
        assert.equal(N3.hasParent(), true);

        assert.equal(N.child, N2);
        assert.equal(N.hasChildren(), true);
        assert.equal(N.degree, 2);

        assert.equal(N.child.next, N3);
        assert.equal(N.child.prev, N3);
        assert.equal(N3.next, N.child);
        assert.equal(N3.prev, N.child);
      });
      it('insertListToCircular()', () => {
        var N: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(1);
        var N1: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(2);
        var N2: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(3);
        N.insertToCircular(N1);
        N.insertToCircular(N2);

        var M: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(10);
        var M1: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(20);
        var M2: FibbonaciHeapNode<number> = new FibbonaciHeapNode<number>(30);
        M.insertToCircular(M1);
        M.insertToCircular(M2);

        N.insertListToCircular(M);

        assert.equal(N.prev.prev.prev.prev.value, 3);
        assert.equal(N.prev.prev.prev.value, 10);
        assert.equal(N.prev.prev.value, 20);
        assert.equal(N.prev.value, 30);
        assert.equal(N.value, 1);
        assert.equal(N.next.value, 2);
        assert.equal(N.next.next.value, 3);
      });
    });
    it('internal representation (used for checking correctness while developing)', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      var min: FibbonaciHeapNode<number> = A.getMinPtr();

      assert.equal(min, null);
      A.insert(1);
      min = A.getMinPtr();
      //circular list
      assert.equal(min.parent, null);
      assert.equal(min.value, 1);
      assert.equal(min.next, min);
      assert.equal(min.prev, min);
      A.insert(2);
      min = A.getMinPtr();
      //circular list
      assert.equal(min.parent, null);
      assert.equal(min.value, 1);
      assert.equal(min.next.value, 2);
      assert.equal(min.prev.value, 2);
      A.insert(3);
      min = A.getMinPtr();
      //circular list
      assert.equal(min.parent, null);
      assert.equal(min.value, 1);
      assert.equal(min.next.value, 2);
      assert.equal(min.prev.value, 3);
    });
  });
  describe('classic test', () => {
    it('find()', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      A.insert(1);
      var ptr2 = A.insert(2);
      var ptr3 = A.insert(3);
      var ptr4 = A.insert(4);
      var ptr5 = A.insert(5);
      //make things interesting
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.find(2), ptr2);
      assert.equal(A.find(3), ptr3);
      assert.equal(A.find(4), ptr4);
      assert.equal(A.find(5), ptr5);
    });
    it('find() #2', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      A.insert(1);
      var ptr01 = A.insert(0);
      var ptr02 = A.insert(0);
      var ptr21 = A.insert(2);
      var ptr22 = A.insert(2);
      assert.equal(A.extractMinMax(), 0);
      //assert.equal(A.decreaseKey(), 0);
      assert.notEqual(A.find(0), null);
    });
    it('increaseKey() - case #1: no heap property violation', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      A.insert(1);
      //save ptr for the future only root for testing tree shape
      var ptr2 = A.insert(2);
      var ptr3 = A.insert(3);
      var ptr4 = A.insert(4);
      var ptr50 = A.insert(5);
      assert.equal(A.extractMinMax(), 1);
      A.increaseKey(ptr50, 50, Number.MIN_VALUE);

      assert.equal(A.extractMinMax(), 2);
      assert.equal(A.extractMinMax(), 3);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.extractMinMax(), 50);
      assert.equal(A.isEmpty(), true);
    });
    it('extractMinMax() + correct tree structure', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      A.insert(1);
      //save ptr for the future only root for testing tree shape
      var ptr2 = A.insert(2);
      var ptr3 = A.insert(3);
      var ptr4 = A.insert(4);
      var ptr5 = A.insert(5);
      assert.equal(A.extractMinMax(), 1);

      A.requireRoots([2]);
      assert.equal(ptr2.hasChildren(), true);
      assert.equal(ptr2.child, ptr3);
      assert.equal(ptr2.child.next, ptr4);
      assert.equal(ptr2.child.prev, ptr4);
      //4 must have child 5
      assert.equal(ptr4.hasChildren(), true);
      assert.equal(ptr4.child, ptr5);
      //the only child
      assert.equal(ptr5.next, ptr5);
      //5 has no child
      assert.equal(ptr5.hasChildren(), false);
    });
    it('decreaseKey() - case #1 (no heap property violation)', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1);
      var nodePtr = A.insert(2);
      var nodePtr2 = A.insert(3);
      A.insert(4);
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.length(), 3);
      A.decreaseKey(nodePtr, 0);
      assert.equal(A.length(), 3);
      A.decreaseKey(nodePtr2, 1);
      assert.equal(A.length(), 3);
      assert.equal(A.extractMinMax(), 0);
      assert.equal(A.length(), 2);
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.length(), 1);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.length(), 0);
    });
    describe('decreaseKey() - case #2 (heap property violation)', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      A.insert(1);
      var ptr2 = A.insert(2);
      var ptr3 = A.insert(3);
      var ptr4 = A.insert(4);
      var ptr0 = A.insert(5);
      assert.equal(A.extractMinMax(), 1);
      A.decreaseKey(ptr0, 0);

      it('correct tree structure', () => {
        //**************************
        A.requireRoots([2, 0]);
        assert.equal(ptr2.hasChildren(), true);
        assert.equal(ptr3.hasChildren(), false);
        assert.equal(ptr4.hasChildren(), false);
        assert.equal(ptr0.hasChildren(), false);
        assert.equal(ptr4.marked, true);
        assert.equal(ptr3.marked, false);
        assert.equal(ptr2.marked, false);
        assert.equal(ptr0.marked, false);

        assert.equal(ptr2.child, ptr3);
        //in this case child is node 3, so use next
        assert.equal(ptr2.child.next, ptr4);
      });
    });
    it('decreaseKey() - case #3 (heap property violation) + correct tree structure', () => {
      var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
      //create heap by hand
      var ptr7 = A.insert(7);
      var ptr24 = new FibbonaciHeapNode<number>(24);
      var ptr26 = new FibbonaciHeapNode<number>(26);
      var ptr88 = new FibbonaciHeapNode<number>(88);
      var ptr5 = new FibbonaciHeapNode<number>(35);

      ptr7.addChild(ptr24);
      ptr24.addChild(ptr26);
      ptr26.addChild(ptr5);
      ptr26.addChild(ptr88);

      //simulate marking to force case 3
      ptr24.marked = true;
      ptr26.marked = true;
      A.decreaseKey(ptr5, 5);

      //correct tree structure
      A.requireRoots([7, 5, 26, 24]);
      assert.equal(ptr26.hasChildren(), true);
      assert.equal(ptr26.child, ptr88);
      assert.equal(ptr88.hasChildren(), false);
      assert.equal(ptr24.hasChildren(), false);
      assert.equal(ptr5.hasChildren(), false);
      assert.equal(ptr7.hasChildren(), false);
      //marks
      assert.equal(ptr5.marked, false);
      assert.equal(ptr7.marked, false);
      assert.equal(ptr24.marked, false);
      assert.equal(ptr26.marked, false);
      assert.equal(ptr88.marked, false);
    });
    it('extractMinMax(), length(), insert(), isEmpty() #1', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1);
      A.insert(2);
      A.insert(3);
      A.insert(4);
      //console.log(A.toString())
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.extractMinMax(), 2);
      assert.equal(A.extractMinMax(), 3);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });
    it('extractMinMax(), length(), insert(), isEmpty() #2', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1);
      A.insert(4);
      A.insert(-1);
      A.insert(3);
      A.insert(2);
      console.log(A.toString())
      assert.equal(A.extractMinMax(), -1);
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.extractMinMax(), 2);
      assert.equal(A.extractMinMax(), 3);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });
    it('extractMinMax(), length(), insert(), isEmpty() #3', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(4);
      A.insert(2);
      A.insert(7);
      A.insert(6);
      A.insert(0);
      A.insert(3);
      A.insert(1);
      A.insert(5);
      assert.equal(A.extractMinMax(), 0);
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.extractMinMax(), 2);
      assert.equal(A.extractMinMax(), 3);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.extractMinMax(), 5);
      assert.equal(A.extractMinMax(), 6);
      assert.equal(A.extractMinMax(), 7);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });
    it('extractMinMax(), length(), insert(), isEmpty() #4', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(3);
      A.insert(2);
      A.insert(20);
      A.insert(10);
      A.insert(4);
      A.insert(0);
      A.insert(6);
      A.insert(5);
      A.insert(1);
      assert.equal(A.extractMinMax(), 0);
      assert.equal(A.extractMinMax(), 1);
      assert.equal(A.extractMinMax(), 2);
      assert.equal(A.extractMinMax(), 3);
      assert.equal(A.extractMinMax(), 4);
      assert.equal(A.extractMinMax(), 5);
      assert.equal(A.extractMinMax(), 6);
      assert.equal(A.extractMinMax(), 10);
      assert.equal(A.extractMinMax(), 20);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });

    it('reconstructed failing random data test #1', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1905);
      A.insert(-54641);
      A.insert(0);
      assert.equal(A.extractMinMax(), -54641);
      assert.equal(A.extractMinMax(), 0);
      assert.equal(A.extractMinMax(), 1905);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });
    it('reconstructed failing random data test #2', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1905);
      A.insert(-54641);
      A.insert(0);
      assert.equal(A.extractMinMax(), -54641);
      assert.equal(A.extractMinMax(), 0);
      assert.equal(A.extractMinMax(), 1905);
      assert.equal(A.length(), 0);
      assert.equal(A.isEmpty(), true);
    });
    it('reconstructed failing random data test #3 (special tree shape with duplicities, which creates so fucking special case...)', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(1);
      A.insert(-2);
      A.insert(-1);
      A.insert(-1);
      A.insert(0);
      assert.equal(A.extractMinMax(), -2);
      assert.equal(A.extractMinMax(), -1);
      assert.equal(A.extractMinMax(), -1);
    });
    it('reconstructed failing random data test #4', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      A.insert(161);
      A.insert(-97);
      A.insert(-143);
      A.insert(-55);
      A.insert(-155);
      A.insert(192);
      A.insert(151);
      A.insert(-172);
      A.insert(-37);
      A.insert(-178);
      A.insert(-140);
      A.insert(-66);
      assert.equal(A.extractMinMax(), -178);
      assert.equal(A.extractMinMax(), -172);
      assert.equal(A.extractMinMax(), -155);
      assert.equal(A.extractMinMax(), -143);
      assert.equal(A.extractMinMax(), -140);
      assert.equal(A.extractMinMax(), -97);
      assert.equal(A.extractMinMax(), -66);
      assert.equal(A.extractMinMax(), -55);
      assert.equal(A.extractMinMax(), -37);
      assert.equal(A.extractMinMax(), 151);
      assert.equal(A.extractMinMax(), 161);
      assert.equal(A.extractMinMax(), 192);
    });
  });
  var suite: HeapTesterSuite = new HeapTesterSuite(comparator);
  describe('random data test (insert all elements, then remove one by one until no one is left)', () => {
    it('small data count, used for debugging...', () => {
      //suite.reportInput();
      for (var i: number = 0; i < 5000; i++) {
        var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
        suite.testWithRandomData(
          function(a: number) {
            A.insert(a);
          },
          function(): number {
            return A.extractMinMax()
          },
          function(): number {
            return A.length()
          },
          5);
      }
    });
    it('data count: 50', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      suite.testWithRandomData(
        function(a: number) {
          A.insert(a);
        },
        function(): number {
          return A.extractMinMax()
        },
        function(): number {
          return A.length()
        },
        50);
    });
    it('data count: 1 000', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      suite.testWithRandomData(
        function(a: number) {
          A.insert(a);
        },
        function(): number {
          return A.extractMinMax()
        },
        function(): number {
          return A.length()
        },
        1000);
    });
    it('data count: 10 000', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      suite.testWithRandomData(
        function(a: number) {
          A.insert(a);
        },
        function(): number {
          return A.extractMinMax()
        },
        function(): number {
          return A.length()
        },
        10000);
    });
    it('data count: 20 000', () => {
      var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
      suite.testWithRandomData(
        function(a: number) {
          A.insert(a);
        },
        function(): number {
          return A.extractMinMax()
        },
        function(): number {
          return A.length()
        },
        10000);
    });
    it('Random amount of data inserted (from 0 to 1000) - (100 tests)', () => {
      for (let i = 0; i < 100; i++) {
        var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
        suite.testWithRandomData(
          function(a: number) {
            A.insert(a);
          },
          function(): number {
            return A.extractMinMax()
          },
          function(): number {
            return A.length()
          },
          1000, true);
      }
    });
    it('Random amount of data inserted (from 0 to 3 000) - (100 tests)', () => {
      for (let i = 0; i < 100; i++) {
        var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
        suite.testWithRandomData(
          function(a: number) {
            A.insert(a);
          },
          function(): number {
            return A.extractMinMax()
          },
          function(): number {
            return A.length()
          },
          3000, true);
      }
    });
    it('Random amount of data inserted (from 0 to 3000) - (5 000 tests)', () => {
      for (let i = 0; i < 5000; i++) {
        var A: FibbonaciMinHeap<number> = new FibbonaciMinHeap<number>(comparator);
        suite.testWithRandomData(
          function(a: number) {
            A.insert(a);
          },
          function(): number {
            return A.extractMinMax()
          },
          function(): number {
            return A.length()
          },
          3000, true);
      }
    });
  });
  it('Random data + randomly chosen operations on the heap ( extractMix / decreaseKey ) - 1000 randomly chosen operations. (can not do alot of data test, because this is heavy as hell)', () => {
    var suiteTank = new FibbonaciHeapRandomTester()
    suiteTank.testWithRandomData(1000);
  });
  it('mixture', () => {
    var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
    A.insert(1);
    A.insert(-1);
    var ptr = A.find(1);
    A.decreaseKey(ptr, -44);
    assert.equal(A.extractMinMax(), -44);
  });
  it('mixture #2', () => {
    var A: FibbonaciHeapMock = new FibbonaciHeapMock(comparator);
    A.insert(-9);
    A.insert(12);
    A.insert(-7);
    A.insert(0);
    A.insert(17);
    A.insert(-3);
    A.insert(2);
    A.insert(-6);
    assert.equal(A.extractMinMax(), -9);
    A.decreaseKey(A.find(0), -22);
    A.decreaseKey(A.find(-3), -25);
    assert.equal(A.extractMinMax(), -25);
    assert.notEqual(A.find(17), null);
    A.decreaseKey(A.find(17), -28);
  });
});

