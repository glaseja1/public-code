# Public code
Here is public code, that I offer to all of you people. 
I guarantee, that all the code is well tested, and I did all I possibly could to make it a masterpiece of my life.

## Fibbonaci heap - v. 1.0.0 - stable
I provide here source code for Fibbonaci heap.
The source file can be found in `./FibbonaciHeap.ts` file, however, make sure to read **how to use** section in this manual.
The example code is standalone, and you do not need anyhing else whatso ever to use it.

I also include test files, which are just for demonstration of how I tested that.
You can not compile that nor run it unless you will implement your own containers such as HashMap and substitute it there.

## What is fibbonaci heap?
Fibbonaci heap is a container, which can act like a priority queue. More reading at wikipedia, [here](https://en.wikipedia.org/wiki/Fibonacci_heap)

## What are practical usages of a fibbonaci heap?
I have used it in my MMORPG game, [Darkness or light](http:dol.glaser.cz) in conjunction with A* path finding algorithm, to improve time complexity 
for priority queue by using fibbonaci heap instead binary heap or binomial heap.

Other usages can be:
Imagine having 100 players, and each one has score in his minigame. Every second, each player's score changes. 
And you want to be always able to see the leading player(the one with max score).
Behind this can be a Fibbonaci max heap, where every time someone's score changes, you call decreaseKey / increase key, and then you can call exract max.

## Why should I use your code? I do not know you! Libraries are better!
Libraries are better, if they are written by proffesionals, and tested thoroughly. Ideally, official libraries, then good luck finding official library containing a fibbonaci heap.

So why my code?
I have spent lifetime perfecting myself in programming and algorithmization.
Main goal of all my code is do not rush, and make thorough tests to ensure my work was done properly, and minimize possibility of mistake.

I include here the tests, along with screenshots, these are random data tests, totaling in bilions of data in total. Who gives you that?! 
Have a look at libraries nowadays, you just have to hope they will work...
I also provide measurements of the functions, which can be seen on [my page](http://dol.glaser.cz)

Documentation -> Benchmarks -> Fibbonaci heap
(I do not include full url, as my web constantly changes....)

Alltough it is not a mathematical proof of correctness, we can see from the graphs, that the time of the functions are behaving as expected

## Test results
![test results](/images/testResults.png)

## Support me
You can support the game I am working on at [indieDB](https://www.indiedb.com/games/darkness-or-light)
Our official site is [here](http://dol.glaser.cz)

## How to use
Navigate to the ./fibbonaciHeap/examples/index.html to see an example of usage.
That HTML loads the .js files that contain the fibbonaci heap.
It is important, that any comparator function you use, must return only and only values 1, 0 or -1, **nothing else**. If the comparator would return any other value, then behavior is **undefined**
